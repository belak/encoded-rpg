#pragma once

#include "screen.hpp"
#include <allegro5/allegro.h>

namespace Director {
	// Load the gamespace
	void init();
	
	// Run the main loop
	void run();

	// Clean up the gamespace
	void cleanup();

	// Resets monitor
	void reset();

	void pushScreen(Screen *s);
	screen_ptr popScreen();
	screen_ptr replaceScreen(Screen *s);

	extern ALLEGRO_DISPLAY *display;
	extern ALLEGRO_EVENT_QUEUE *input;
	extern ALLEGRO_CONFIG *config;

	extern float targetFPS;
	extern int width;
	extern int height;
	extern bool fullscreen;
	extern int monitor;
	extern int refresh_rate;
	extern bool running;

	extern float bgmVolume;
	extern float sfxVolume;
};