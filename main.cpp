#include "director.hpp"
//#include "screens/splash_screen.hpp"
#include "screens/main_menu.hpp"

int main(int argc, char *argv[]) {
	Director::init();
	//Director::pushScreen(new SplashScreen());
	Director::pushScreen(new MainMenuScreen());
	Director::run();
	Director::cleanup();

	return 0;
}
