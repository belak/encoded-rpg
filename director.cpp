#include "director.hpp"
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>

#include <stack>
#include <iostream>

using namespace std;

// TODO: constant for conf file name

namespace Director {
	// Public
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *input = NULL;
	ALLEGRO_CONFIG *config = NULL;

	float targetFPS = 60.0;
	int width = 0;
	int height = 0;
	int monitor = 0;
	int refresh_rate = 0;

	// TODO: Load these from config
	float bgmVolume = 1.0;
	float sfxVolume = 1.0;

	bool running = true;
	bool fullscreen = false;

	// Private
	stack<screen_ptr> *screens = NULL;
	ALLEGRO_EVENT_QUEUE *events = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_PATH *config_path = NULL;

	// TODO: Return success or failure
	void init() {
		if (!al_init()) {
			cout << "Allegro failed to start" << endl;
			running = false;
			return;
		}

		// Load extra modules - this only needs to be done once
		// NOTE: Some of these, we should check the return value
		if (!al_install_keyboard()) {
			cout << "keyboard error" << endl;
			running = false;
			return;
		}

		if (!al_install_mouse()) {
			cout << "mouse error" << endl;
			running = false;
			return;
		}

		if (!al_install_audio()) {
			cout << "audio error" << endl;
			running = false;
			return;
		}

		al_init_image_addon();
		al_init_font_addon();
		al_init_ttf_addon();
		al_init_primitives_addon();
		al_init_acodec_addon();

		if(!(events = al_create_event_queue())) {
			cout << "Failed to create event queue" << endl;
			running = false;
			return;
		}

		if(!(input = al_create_event_queue())) {
			cout << "Failed to create input queue" << endl;
			running = false;
			return;
		}

		config_path = al_get_standard_path(ALLEGRO_USER_SETTINGS_PATH);
		al_set_path_filename(config_path, "settings.ini");

		config = al_load_config_file(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP));
		if (!config) {
			config = al_create_config();
		}

		al_add_config_section(config, "display");

		const char *monitor_num = NULL;
		if (!(monitor_num = al_get_config_value(config, "display", "monitor"))) {
			al_set_config_value(config, "display", "monitor", "0");
			monitor = 0;
		} else {
			monitor = atoi(monitor_num);
			if (monitor < 0 || monitor > al_get_num_video_adapters() - 1) {
				al_set_config_value(config, "display", "monitor", "0");
				monitor = 0;
			}
		}

		al_set_new_display_adapter(monitor);

		ALLEGRO_MONITOR_INFO monitor_info;
		if (!al_get_monitor_info(monitor, &monitor_info)) {
			running = false;
			return;
		}

		const char *temp = NULL;
		if (!(temp = al_get_config_value(config, "display", "width"))) {
			char *temp = NULL;
			width = monitor_info.x2 - monitor_info.x1;
			asprintf(&temp, "%d", width);
			al_set_config_value(config, "display", "width", temp);
			free(temp);
		} else {
			width = atoi(temp);
			if (width < 0 || width > monitor_info.x2 - monitor_info.x1) {
				// TODO: warning here?
				char *temp = NULL;
				width = monitor_info.x2 - monitor_info.x1;
				asprintf(&temp, "%d", width);
				al_set_config_value(config, "display", "width", temp);
				free(temp);
			}
		}

		if (!(temp = al_get_config_value(config, "display", "height"))) {
			char *temp = NULL;
			height = monitor_info.y2 - monitor_info.y1;
			asprintf(&temp, "%d", height);
			al_set_config_value(config, "display", "height", temp);
			free(temp);
		} else {
			height = atoi(temp);
			if (height < 0 || height > monitor_info.y2 - monitor_info.y1) {
				// TODO: warning here?
				char *temp = NULL;
				height = monitor_info.y2 - monitor_info.y1;
				asprintf(&temp, "%d", height);
				al_set_config_value(config, "display", "height", temp);
				free(temp);
			}
		}

		if (!(temp = al_get_config_value(config, "display", "fullscreen"))) {
			fullscreen = true;
			al_set_config_value(config, "display", "fullscreen", "1");
		} else {
			// parse true/false
			if (temp[0] == '1' && temp[1] == '\0') {
				fullscreen = true;
			}
		}

		al_set_path_filename(config_path, "");

		// cout << al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP) << endl;

		reset();

		if(!(timer = al_create_timer(1.0f / targetFPS))) {
			cout << "Failed to create allegro timer" << endl;
			running = false;
			return;
		}

		// Register input locations
		al_register_event_source(events, al_get_display_event_source(display));
		al_register_event_source(events, al_get_timer_event_source(timer));

		al_register_event_source(input, al_get_keyboard_event_source());
		al_register_event_source(input, al_get_mouse_event_source());

		al_start_timer(timer);

		screens = new stack<screen_ptr>();
	}

	void reset() {
		if (display) {
			al_destroy_display(display);
		}

		al_set_new_display_adapter(monitor);

		// TODO: Fix this
		if (refresh_rate == 0) {
			refresh_rate = al_get_new_display_refresh_rate();
		}

		al_set_new_display_refresh_rate(refresh_rate);

		if (fullscreen) {
			al_set_new_display_flags(ALLEGRO_FULLSCREEN);
		} else {
			al_set_new_display_flags(0);
		}

		// Make sure to load display before everything else
		if (!(display = al_create_display(width, height))) {
			cout << "Failed to create allegro display" << endl;
			running = false;
			return;
		}

		// Reregister the screen for the close event
		al_register_event_source(events, al_get_display_event_source(display));
	}

	void run() {
		while (running) {
			ALLEGRO_EVENT ev;
			al_wait_for_event(events, &ev);

			if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
				running = false;
				return;
			} else {
				screens->top()->update(ev);
			}

			// We check if the event queue is empty so we don't try to render more times than we can handle
			if (running && al_is_event_queue_empty(events)) {
				al_clear_to_color(al_map_rgb(0, 0, 0));
				screens->top()->render();

				al_flip_display();
			}
		}
	}

	void cleanup() {
		while (!screens->empty()) {
			screens->pop();
		}

		// TODO: Make sure width and height are written to file

		// Do our best to save the conf file
		if (al_make_directory(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP))) {
			al_set_path_filename(config_path, "settings.ini");
			if (!al_save_config_file(al_path_cstr(config_path, ALLEGRO_NATIVE_PATH_SEP), config)) {
				cout << "config file failed to save" << endl;
			}
			al_set_path_filename(config_path, "");
		} else { 
			cout << "could not create dir for config" << endl;
		}

		al_destroy_path(config_path);
		config_path = NULL;
	}

	void pushScreen(Screen *s) {
		screens->push(screen_ptr(s));
	}

	screen_ptr popScreen() {
		if (screens->empty()) {
			screen_ptr temp = shared_ptr<Screen>(nullptr);
			return temp;
		}

		screen_ptr temp = screens->top();
		screens->pop();

		return temp;
	}

	screen_ptr peekScreen() {
		return screens->top();
	}


	screen_ptr replaceScreen(Screen *s) {
		screen_ptr temp = popScreen();
		pushScreen(s);
		return temp;
	}
}