#include "splash_screen.hpp"
#include "main_menu.hpp"

#include "../director.hpp"

#include <iostream>

using namespace std;

SplashScreen::SplashScreen() : Screen() {
	cout << "splash screen created" << endl;
	//this->splash = Graphics::get("logo");
	this->timer = 0.0f;
}

SplashScreen::~SplashScreen() {
	cout << "splash screen destroy" << endl;
}

void SplashScreen::update(ALLEGRO_EVENT &ev) {
	if (ev.type == ALLEGRO_EVENT_TIMER) {
		this->timer += 1.0f / Director::targetFPS;
	}

	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_CHAR) {
			Director::replaceScreen(new MainMenuScreen());
			return;
		}
	}

	if (this->timer >= 2.0f) {
		Director::replaceScreen(new MainMenuScreen());
		return;
	}
}

void SplashScreen::render() {
	/*al_draw_bitmap(
			this->splash,
			(Graphics::screenWidth() - al_get_bitmap_width(this->splash)) / 2,
			(Graphics::screenHeight() - al_get_bitmap_height(this->splash)) / 2,
			0
		);*/
}