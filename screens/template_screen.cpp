#include "template_screen.hpp"
#include "../director.hpp"

#include <iostream>

using namespace std;

TemplateScreen::TemplateScreen() : Screen() {
	cout << "Template screen created" << endl;
}

TemplateScreen::~TemplateScreen() {
	cout << "Template screen destroy" << endl;
}

void TemplateScreen::tick(ALLEGRO_EVENT &ev) {
	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_CHAR) {
			// TODO: Do something
		}
	}
}

void TemplateScreen::render() {

}