#include "main_menu.hpp"
#include "map_screen.hpp"
#include "options_screen.hpp"
#include "../director.hpp"

#include <allegro5/allegro_ttf.h>

#include <iostream>
#include <sstream>

using namespace std;

MainMenuScreen::MainMenuScreen() : Screen(), items() {
	cout << "main menu screen created" << endl;

	title_font = al_load_font("assets/fonts/emulogic.ttf", 32, 0);
	font = al_load_font("assets/fonts/emulogic.ttf", 24, 0);

	items.push_back("New Game");
	items.push_back("Load Game");
	items.push_back("Options");
	items.push_back("Exit");
}

MainMenuScreen::~MainMenuScreen() {
	cout << "main menu screen destroy" << endl;

	al_destroy_font(title_font);
	al_destroy_font(font);
}

void MainMenuScreen::update(ALLEGRO_EVENT &ev) {
	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_CHAR) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				selected = (selected == 0) ? items.size() - 1 : selected - 1;
				if (!save_available && selected == 1) {
					selected = (selected == 0) ? items.size() - 1 : selected - 1;
				}
				break;
			case ALLEGRO_KEY_DOWN:
				selected = (selected + 1) % items.size();
				if (!save_available && selected == 1) {
					selected = (selected + 1) % items.size();
				}
				break;
			case ALLEGRO_KEY_ENTER:
			case ALLEGRO_KEY_SPACE:
				switch (selected) {
					case 0:
						Director::replaceScreen(new MapScreen("assets/maps/main.tmx"));
						break;
					case 1:
						break;
					case 2:
						Director::replaceScreen(new OptionsScreen());
						break;
					case 3:
						Director::running = false;
					default:
						break;
				}
				break;
			}

			// Reset the fade on keypress
			selected_weight = 255;
			selected_shrinking = true;
		}
	}

	if (selected_shrinking) {
		selected_weight -= 5;
		if (selected_weight < 0) {
			selected_weight = 0;
			selected_shrinking = false;
		}
	} else {
		selected_weight += 5;
		if (selected_weight > 255) {
			selected_weight = 255;
			selected_shrinking = true;
		}
	}
}

void MainMenuScreen::render() {
	// TODO: get rid of hard coded pixel values
	al_draw_text(title_font, al_map_rgb(255,255,255), 0, 0, ALLEGRO_ALIGN_LEFT, "Encoded");
	
	ALLEGRO_COLOR color;

	for (int i = 0; i < items.size(); i++) {
		if (!save_available && i == 1) {
			color = al_map_rgb(128, 128, 128);
		} else if (selected == i) {
			color = al_map_rgb(selected_weight, selected_weight, selected_weight);
		} else {
			color = al_map_rgb(255, 255, 255);
		}

		al_draw_text(font, color, Director::width / 2, Director::height * 4 / 7 + al_get_font_line_height(font) * i, ALLEGRO_ALIGN_CENTRE, items[i].c_str());
	}	
}