#pragma once

#include "../screen.hpp"

class TemplateScreen : public Screen {
public:
	TemplateScreen();
	~TemplateScreen();
	void tick(ALLEGRO_EVENT &ev);
	void render();
};