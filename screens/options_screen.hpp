#pragma once

#include "../screen.hpp"

#include <allegro5/allegro_font.h>

#include <string>

class OptionsScreen : public Screen {
public:
	OptionsScreen();
	~OptionsScreen();
	void update(ALLEGRO_EVENT &ev);
	void render();

private:
	void load();
	void unload();
	
	void reset();

	ALLEGRO_FONT *title_font = NULL;
	ALLEGRO_FONT *font = NULL;

	int selected_weight = 255;
	bool selected_shrinking = true;

	int selected = 0;

	int mode = 0;

	std::string current_resolution;
};