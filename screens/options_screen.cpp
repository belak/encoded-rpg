#include "options_screen.hpp"
#include "main_menu.hpp"
#include "../director.hpp"

#include <allegro5/allegro_ttf.h>

#include <iostream>
#include <sstream>

using namespace std;

OptionsScreen::OptionsScreen() : Screen() {
	cout << "options screen created" << endl;

	this->load();
	
	ostringstream ss;
	ss << Director::width << "x" << Director::height << "@" << Director::refresh_rate;
	current_resolution = ss.str();
}

OptionsScreen::~OptionsScreen() {
	cout << "options screen destroy" << endl;

	this->unload();
}

void OptionsScreen::load() {
	title_font = al_load_font("assets/fonts/emulogic.ttf", 32, 0);
	font = al_load_font("assets/fonts/emulogic.ttf", 24, 0);
}

void OptionsScreen::unload() {
	al_destroy_font(title_font);
	al_destroy_font(font);
}

void OptionsScreen::reset() {
	this->unload();

	ALLEGRO_DISPLAY_MODE temp;
	al_get_display_mode(this->mode, &temp);

	Director::width = temp.width;
	Director::height = temp.height;
	Director::refresh_rate = temp.refresh_rate;

	ostringstream ss;
	ss << Director::width << "x" << Director::height << "@" << Director::refresh_rate;
	current_resolution = ss.str();

	Director::reset();

	this->load();
}

void OptionsScreen::update(ALLEGRO_EVENT &ev) {
	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_CHAR) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_UP:
				selected = (selected == 0) ? 2 : selected - 1;
				break;
			case ALLEGRO_KEY_DOWN:
				selected = (selected + 1) % 3;
				break;
			case ALLEGRO_KEY_LEFT:
				if (selected == 0) {
					this->mode--;
					if (this->mode < 0) {
						this->mode = al_get_num_display_modes() - 1;
					}
					this->reset();
				} else if (selected == 1) {
					Director::bgmVolume -= 0.1;
					if (Director::bgmVolume < 0.0) {
						Director::bgmVolume = 0;
					}
				} else if (selected == 2) {
					Director::sfxVolume -= 0.1;
					if (Director::sfxVolume < 0) {
						Director::sfxVolume = 0;
					}
				}
				break;
			case ALLEGRO_KEY_RIGHT:
				if (selected == 0) {
					this->mode = (this->mode + 1) % al_get_num_display_modes();
					this->reset();
				} else if (selected == 1) {
					Director::bgmVolume += 0.1;
					if (Director::bgmVolume > 2.0) {
						Director::bgmVolume = 2.0;
					}
				} else if (selected == 2) {
					Director::sfxVolume += 0.1;
					if (Director::sfxVolume > 2.0) {
						Director::sfxVolume = 2.0;
					}
				}
				break;
			case ALLEGRO_KEY_ENTER:
			case ALLEGRO_KEY_SPACE:
				if (selected == 0) {
					this->mode = (this->mode + 1) % al_get_num_display_modes();
					this->reset();
				}
				break;
			case ALLEGRO_KEY_ESCAPE:
				Director::replaceScreen(new MainMenuScreen());
				break;
			}

			// Reset the fade on keypress
			selected_weight = 255;
			selected_shrinking = true;
		}
	}

	if (selected_shrinking) {
		selected_weight -= 5;
		if (selected_weight < 0) {
			selected_weight = 0;
			selected_shrinking = false;
		}
	} else {
		selected_weight += 5;
		if (selected_weight > 255) {
			selected_weight = 255;
			selected_shrinking = true;
		}
	}
}

void OptionsScreen::render() {
	// TODO: get rid of hard coded pixel values
	// TODO: get rid of repeditive code
	al_draw_text(title_font, al_map_rgb(255,255,255), 0, 0, ALLEGRO_ALIGN_LEFT, "Options");
	ALLEGRO_COLOR color;
	if (selected == 0) {
		color = al_map_rgb(selected_weight, selected_weight, selected_weight);
	} else {
		color = al_map_rgb(255, 255, 255);
	}

	al_draw_text(font, al_map_rgb(255, 255, 255), 0, Director::height / 4, ALLEGRO_ALIGN_LEFT, "Resolution");
	al_draw_text(font, color, Director::width / 2, Director::height / 4, ALLEGRO_ALIGN_CENTRE, current_resolution.c_str());

	if (selected == 1) {
		color = al_map_rgb(selected_weight, selected_weight, selected_weight);
	} else {
		color = al_map_rgb(255, 255, 255);
	}
	al_draw_text(font, al_map_rgb(255, 255, 255), 0, Director::height / 4 + 100, ALLEGRO_ALIGN_LEFT, "BGM Volume");
	al_draw_textf(font, color, Director::width / 2, Director::height / 4 + 100, ALLEGRO_ALIGN_CENTRE, "%.1f%%", Director::bgmVolume * 50);

	if (selected == 2) {
		color = al_map_rgb(selected_weight, selected_weight, selected_weight);
	} else {
		color = al_map_rgb(255, 255, 255);
	}
	al_draw_text(font, al_map_rgb(255, 255, 255), 0, Director::height / 4 + 200, ALLEGRO_ALIGN_LEFT, "SFX Volume");
	al_draw_textf(font, color, Director::width / 2, Director::height / 4 + 200, ALLEGRO_ALIGN_CENTRE, "%.1f%%", Director::sfxVolume * 50);
}