#include "map_screen.hpp"
#include "../director.hpp"

#include <allegro5/allegro_primitives.h>

#include <iostream>

using namespace std;

MapScreen::MapScreen(std::string filename) : Screen(), last_direction(), map() {
	cout << "map screen created" << endl;
	this->timer = 0.0f;
	map.ParseFile(filename);

	// Load the tileset's graphics
	for (int i = 0; i < map.GetNumTilesets(); ++i) {
		const Tmx::Tileset *tileset = map.GetTileset(i);
		string filename = "assets/maps/" + tileset->GetImage()->GetSource();
		ALLEGRO_BITMAP *tilesetImg = al_load_bitmap(filename.c_str());
		tilesets.push_back(tilesetImg);

		// Create sub-bitmaps for each tile in the tileset
		int tilesInRow = (tileset->GetImage()->GetWidth() - 2 * tileset->GetMargin() + tileset->GetSpacing()) / (tileset->GetTileWidth() + tileset->GetSpacing());
		int tilesInCol = (tileset->GetImage()->GetHeight() - 2 * tileset->GetMargin() + tileset->GetSpacing()) / (tileset->GetTileHeight() + tileset->GetSpacing());

		// Create a sub-bitmap for each tile
		for (int x = 0; x < tilesInRow; ++x) {
			for (int y = 0; y < tilesInCol; ++y) {
				// The starting x and y for each tile
				int px = tileset->GetMargin() + x * (tileset->GetSpacing() + tileset->GetTileWidth());
				int py = tileset->GetMargin() + y * (tileset->GetSpacing() + tileset->GetTileHeight());

				// Create the sub-bitmap
				ALLEGRO_BITMAP *tile = al_create_sub_bitmap(tilesetImg, px, py, tileset->GetTileWidth(), tileset->GetTileHeight());
				
				// Add the bitmap to the tiles map
				int id = y * tilesInRow + x + tileset->GetFirstGid();
				tiles[id] = tile;
			}
		}
	}

	tiles_to_draw_x = Director::width / map.GetTileWidth();
	tiles_to_draw_y = Director::height / map.GetTileHeight();
}

MapScreen::~MapScreen() {
	cout << "map screen destroy" << endl;
	// Free the sub-bitmaps
	// NOTE: This has to be done before the tilesets are freed or bad things will happen
	for (std::pair<int, ALLEGRO_BITMAP *> temp : tiles) {
		ALLEGRO_BITMAP *tileset = temp.second;
		al_destroy_bitmap(tileset);
	}
	tiles.clear();

	// Free the tilesets
	while (!tilesets.empty()) {
		ALLEGRO_BITMAP *tileset = tilesets.back();
		tilesets.pop_back();
		al_destroy_bitmap(tileset);
	}
}

void MapScreen::update(ALLEGRO_EVENT &ev) {
	// TODO: Less duplication
	int start_x = offset_x / map.GetTileWidth();
	int start_y = offset_y / map.GetTileHeight();
	//int end_x = start_x + tiles_to_draw_x;
	//int end_y = start_y + tiles_to_draw_y;

	// On each tick, we run through all the input
	while (!al_is_event_queue_empty(Director::input)) {
		ALLEGRO_EVENT iev;
		al_wait_for_event(Director::input, &iev);
		if (iev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_RIGHT:
			case ALLEGRO_KEY_LEFT:
			case ALLEGRO_KEY_DOWN:
			case ALLEGRO_KEY_UP:
				last_direction.push_back(iev.keyboard.keycode);
				break;
			}
		} else if (iev.type == ALLEGRO_EVENT_KEY_UP) {
			switch (iev.keyboard.keycode) {
			case ALLEGRO_KEY_RIGHT:
			case ALLEGRO_KEY_LEFT:
			case ALLEGRO_KEY_DOWN:
			case ALLEGRO_KEY_UP:
				last_direction.remove(iev.keyboard.keycode);
				break;
			}
		}
	}

	if (cur_direction != 0 && tween_x == 0 && tween_y == 0) {
		cur_direction = 0;
	}

	if (!last_direction.empty() && tween_x == 0 && tween_y == 0) {
		cur_direction = last_direction.back();
	}

	switch (cur_direction) {
	case ALLEGRO_KEY_RIGHT:
		if (char_x < map.GetWidth() - 1 && tween_x == 0 && tween_y == 0) {
			char_x += 1;
			tween_x -= map.GetTileWidth();
		}
		if (char_x - start_x > tiles_to_draw_x / 2 && offset_x + Director::width < map.GetWidth() * map.GetTileWidth()) {
			offset_x += map.GetTileWidth();
			map_tween_x -= map.GetTileWidth();
		}
		if (tween_x != 0) {
			tween_x += movement_x;
		}
		if (map_tween_x != 0) {
			map_tween_x += movement_x;
		}
		break;
	case ALLEGRO_KEY_LEFT:
		if (char_x > 0 && tween_x == 0 && tween_y == 0) {
			char_x -= 1;
			tween_x += map.GetTileWidth();
		}
		if (char_x - start_x + 1 < tiles_to_draw_x / 2 && offset_x > 0) {
			offset_x -= map.GetTileWidth();
			map_tween_x += map.GetTileWidth();
		}
		if (tween_x != 0) {
			tween_x -= movement_x;
		}
		if (map_tween_x != 0) {
			map_tween_x -= movement_x;
		}
		break;
	case ALLEGRO_KEY_DOWN:
		if (char_y < map.GetHeight() - 1 && tween_x == 0 && tween_y == 0) {
			char_y += 1;
			tween_y -= map.GetTileHeight();
		}
		if (char_y - start_y > tiles_to_draw_y / 2 && offset_y + Director::height < map.GetHeight() * map.GetTileHeight()) {
			offset_y += map.GetTileHeight();
			map_tween_y -= map.GetTileWidth();
		}
		if (tween_y != 0) {
			tween_y += movement_y;
		}
		if (map_tween_y != 0) {
			map_tween_y += movement_y;
		}
		break;
	case ALLEGRO_KEY_UP:
		if (char_y > 0 && tween_x == 0 && tween_y == 0) {
			char_y -= 1;
			tween_y += map.GetTileHeight();
		}
		if (char_y - start_y + 1 < tiles_to_draw_y / 2 && offset_y > 0) {
			offset_y -= map.GetTileHeight();
			map_tween_y += map.GetTileWidth();
		}
		if (tween_y != 0) {
			tween_y -= movement_y;
		}
		if (map_tween_y != 0) {
			map_tween_y -= movement_y;
		}
		break;
	}
}

void MapScreen::render() {
	// Clear to black
	al_clear_to_color(al_map_rgb(0, 0, 0));

	for (int i = 0; i < map.GetNumLayers(); ++i) {
		// Get a layer.
		const Tmx::Layer *layer = map.GetLayer(i);

		// Only render the layer if it's visible
		if (layer->IsVisible()) {

			int start_x = offset_x / map.GetTileWidth();
			int start_y = offset_y / map.GetTileHeight();
			int end_x = start_x + tiles_to_draw_x;
			int end_y = start_y + tiles_to_draw_y;

			if (Director::width % map.GetTileWidth() != 0) {
				end_x += 1;
			}

			if (Director::height % map.GetTileHeight() != 0) {
				end_y += 1;
			}

			int temp_start_x = start_x;
			int temp_end_x = end_x;
			if (map_tween_x != 0) {
				if (cur_direction == ALLEGRO_KEY_RIGHT && start_x > 0) {
					temp_start_x -= 1;
				} else {
					temp_end_x += 1;
				}
			}

			int temp_start_y = start_y;
			int temp_end_y = end_y;
			if (map_tween_y != 0) {
				if (cur_direction == ALLEGRO_KEY_DOWN && start_y > 0) {
					temp_start_y -= 1;
				} else {
					temp_end_y += 1;
				}
			}

			for (int x = temp_start_x; x < layer->GetWidth() && x < temp_end_x; ++x) {
				for (int y = temp_start_y; y < layer->GetHeight() && y < temp_end_y; ++y) {
					const Tmx::MapTile &mapTile = layer->GetTile(x, y);

					if (mapTile.tilesetId >= 0) {
						int flags = 0;
						float angle = 0;

						const Tmx::Tileset *tileset = map.GetTileset(mapTile.tilesetId);

						int tileId = mapTile.id + tileset->GetFirstGid();

						if (layer->IsTileFlippedHorizontally(x, y)) {
							flags |= ALLEGRO_FLIP_HORIZONTAL;
						}

						if (layer->IsTileFlippedVertically(x, y)) {
							flags |= ALLEGRO_FLIP_VERTICAL;
						}

						if (layer->IsTileFlippedDiagonally(x, y)){
							flags ^= ALLEGRO_FLIP_HORIZONTAL;
							angle = ALLEGRO_PI / 2.0;
						}

						int dx = (x - start_x) * tileset->GetTileWidth();
						int dy = (y - start_y) * tileset->GetTileHeight();

						ALLEGRO_BITMAP *tile = tiles[tileId];

						if (tile != NULL) {
							float width = 1.0 * tileset->GetTileWidth() / 2;
							float height = 1.0 * tileset->GetTileHeight() / 2;

							al_draw_rotated_bitmap(
								tile,
								width, height,
								dx - map_tween_x + width, dy - map_tween_y + height, angle, flags);
						}
					}
				}
			}

			al_draw_rectangle(
				(char_x - start_x) * map.GetTileWidth() + tween_x - map_tween_x + 1,
				(char_y - start_y) * map.GetTileHeight() + tween_y - map_tween_y + 1,
				(char_x - start_x + 1) * map.GetTileWidth() + tween_x - map_tween_x,
				(char_y - start_y + 1) * map.GetTileHeight() + tween_y - map_tween_y,
				al_map_rgb(0, 255, 0),
				1);
		}
	}
}