#pragma once

#include "../screen.hpp"

class SplashScreen : public Screen {
public:
	SplashScreen();
	~SplashScreen();
	void update(ALLEGRO_EVENT &ev);
	void render();
private:
	float timer = 0.0f;
	//ALLEGRO_BITMAP *splash = NULL;
};