#pragma once

#include "../screen.hpp"

#include <map>
#include <vector>
#include <list>

#include <TmxParser/Tmx.h>

class MapScreen : public Screen {
public:
	MapScreen(std::string filename);
	~MapScreen();
	void update(ALLEGRO_EVENT &ev);
	void render();
private:
	float timer = 0.0f;

	int offset_x = 0;
	int offset_y = 0;
	int map_tween_x = 0;
	int map_tween_y = 0;

	int movement_x = 4;
	int movement_y = 4;

	int cur_direction = 0;

	int char_x = 0;
	int char_y = 0;
	int tween_x = 0;
	int tween_y = 0;

	int tiles_to_draw_x = 0;
	int tiles_to_draw_y = 0;

	std::list<int> last_direction;
	
	// The XML map
	Tmx::Map map;

	// Tilesets
	std::vector<ALLEGRO_BITMAP *> tilesets;

	// Sub-Bitmaps, representing each separate tile
	std::map<int, ALLEGRO_BITMAP *> tiles;
};